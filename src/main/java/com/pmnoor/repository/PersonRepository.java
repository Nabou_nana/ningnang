package com.pmnoor.repository;

import org.springframework.data.repository.CrudRepository;

import com.pmnoor.model.Person;

public interface PersonRepository extends CrudRepository<Person, Integer>  {
	

}
