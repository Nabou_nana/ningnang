package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PmnoorApplication {

	public static void main(String[] args) {
		SpringApplication.run(PmnoorApplication.class, args);
	}

}
