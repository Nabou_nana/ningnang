FROM maven:3.6.3-jdk-11-slim AS MAVEN_BUILD
2
#FROM maven:3.5.2-jdk-8-alpine AS MAVEN_BUILD FOR JAVA 8
3
4
ARG SPRING_ACTIVE_PROFILE
5
6
MAINTAINER noor
7
COPY pom.xml /build/
8
COPY src /build/src/
9
WORKDIR /build/
10
RUN mvn clean install -Dspring.profiles.active=$SPRING_ACTIVE_PROFILE && mvn package -B -e -Dspring.profiles.active=$SPRING_ACTIVE_PROFILE
11
FROM openjdk:11-slim
12
#FROM openjdk:8-alpine FOR JAVA 8
13
WORKDIR /ningnang
14
15
COPY --from=MAVEN_BUILD /build/target/ningnang-*.jar /pmnoor/ningnang.jar
16
ENTRYPOINT ["java", "-jar", "ningnang.jar"]